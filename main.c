#include <errno.h>
#include <inttypes.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>



void posix_error(int code, char *msg){

	fprintf(stderr,"%s: %s\n", strerror(code), msg);
	exit(EXIT_FAILURE);
}

/* Robert Jenkins' 96 bit Mix Function
 * burtleburtle.net/bob/hash/doobs.html */
uint32_t mix96(uint32_t a, uint32_t b, uint32_t c){

    a=a-b;  a=a-c;  a=a^(c >> 13);
    b=b-c;  b=b-a;  b=b^(a << 8);
    c=c-a;  c=c-b;  c=c^(b >> 13);
    a=a-b;  a=a-c;  a=a^(c >> 12);
    b=b-c;  b=b-a;  b=b^(a << 16);
    c=c-a;  c=c-b;  c=c^(b >> 5);
    a=a-b;  a=a-c;  a=a^(c >> 3);
    b=b-c;  b=b-a;  b=b^(a << 10);
    c=c-a;  c=c-b;  c=c^(b >> 15);
    return c;
}

uint32_t getRand( void ){

  return (uint32_t)rand();
}

uint32_t rollNDie( uint32_t rollCount, uint32_t diceMod ){

  uint32_t rollVal, i;

  for (i = 0; i < rollCount; i++){
    rollVal += (getRand() % diceMod) + 1;
  }

  return rollVal;
}

void parseOpt(char *stropt) {

  regex_t 	rgx_syntaxCheck, rgx_firstValue, rgx_secondValue;
  regmatch_t 	pmatch1[8], pmatch2[8];
  int 	syntaxCheck, reti, retj, i, rollCount, diceMod;
  char 	errbuf[128];

  // ^\d+[dD]\d+$   ==   ^[[:digit:]]+[dD][[:digit:]]+$
  if (syntaxCheck = regcomp(&rgx_syntaxCheck, "^[[:digit:]]+[dD][[:digit:]]+$", REG_EXTENDED))
    posix_error(EAGAIN, "Error in Regex call.");;
  reti = regcomp(&rgx_firstValue, "^[[:digit:]]+", REG_EXTENDED);
  retj = regcomp(&rgx_secondValue, "[[:digit:]]+$", REG_EXTENDED);

  syntaxCheck = regexec(&rgx_syntaxCheck, stropt, 0, NULL, 0);
  reti = regexec(&rgx_firstValue, stropt, 8, &pmatch1, 0);
  retj = regexec(&rgx_secondValue, stropt, 8, &pmatch2, 0);

  if (syntaxCheck == REG_NOMATCH || reti == REG_NOMATCH || retj == REG_NOMATCH)

    posix_error(EINVAL, "Invalid dice statement. Format: #d#");

  else if(syntaxCheck || (!reti || !retj)) {

    char *results[1], *cbuff = malloc(2*sizeof(char));
    results[0] = calloc(sizeof(char)*(pmatch1[0].rm_eo - pmatch1[0].rm_so), 0);
    results[1] = calloc(sizeof(char)*(pmatch2[0].rm_eo - pmatch2[0].rm_so), 0);

    for (i = pmatch1[0].rm_so; i < pmatch1[0].rm_eo; i++){
      sprintf(results[0], "%s%c", results[0], stropt[i]); // I know what I've done.
    }

    for (i = pmatch2[0].rm_so; i < pmatch2[0].rm_eo; i++){
      sprintf(results[1], "%s%c", results[1], stropt[i]); // Don't think any less of me.
    }

    //printf("Results: %s %s\n", results[0], results[1]);

    uint32_t rollCount = strtol(results[0], NULL, 10), diceMod = strtol(results[1], NULL, 10);

    if ( rollCount == 0 || diceMod == 0 )
      posix_error(EINVAL, "Dice count and mod values must exceed 0.");
    else
      printf("%d\n", rollNDie(strtol(results[0], NULL, 10), strtol(results[1], NULL, 10)));

  } else {

    regerror(reti, &reti, errbuf, sizeof(errbuf));
    posix_error(EINVAL, errbuf);
  }
}

int main(int argc, char* argv[]) {

  // seed rng
  srand(mix96(clock(), time(NULL), getpid()));

  if (argc > 1) parseOpt(argv[1]);
  else posix_error(EINVAL, "Provide a dice statement. Format: #d#");

  return EXIT_SUCCESS;
}
